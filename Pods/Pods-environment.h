
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Appirater
#define COCOAPODS_POD_AVAILABLE_Appirater
#define COCOAPODS_VERSION_MAJOR_Appirater 2
#define COCOAPODS_VERSION_MINOR_Appirater 0
#define COCOAPODS_VERSION_PATCH_Appirater 2

// ColorPopover
#define COCOAPODS_POD_AVAILABLE_ColorPopover
#define COCOAPODS_VERSION_MAJOR_ColorPopover 0
#define COCOAPODS_VERSION_MINOR_ColorPopover 0
#define COCOAPODS_VERSION_PATCH_ColorPopover 1

// GPUImage
#define COCOAPODS_POD_AVAILABLE_GPUImage
#define COCOAPODS_VERSION_MAJOR_GPUImage 0
#define COCOAPODS_VERSION_MINOR_GPUImage 1
#define COCOAPODS_VERSION_PATCH_GPUImage 2

// GRKGradientView
#define COCOAPODS_POD_AVAILABLE_GRKGradientView
#define COCOAPODS_VERSION_MAJOR_GRKGradientView 1
#define COCOAPODS_VERSION_MINOR_GRKGradientView 0
#define COCOAPODS_VERSION_PATCH_GRKGradientView 1

// GoogleAnalytics-iOS-SDK
#define COCOAPODS_POD_AVAILABLE_GoogleAnalytics_iOS_SDK
#define COCOAPODS_VERSION_MAJOR_GoogleAnalytics_iOS_SDK 3
#define COCOAPODS_VERSION_MINOR_GoogleAnalytics_iOS_SDK 0
#define COCOAPODS_VERSION_PATCH_GoogleAnalytics_iOS_SDK 3

// JSONKit-NoWarning
#define COCOAPODS_POD_AVAILABLE_JSONKit_NoWarning
#define COCOAPODS_VERSION_MAJOR_JSONKit_NoWarning 1
#define COCOAPODS_VERSION_MINOR_JSONKit_NoWarning 1
#define COCOAPODS_VERSION_PATCH_JSONKit_NoWarning 0

// LEColorPicker
#define COCOAPODS_POD_AVAILABLE_LEColorPicker
#define COCOAPODS_VERSION_MAJOR_LEColorPicker 1
#define COCOAPODS_VERSION_MINOR_LEColorPicker 0
#define COCOAPODS_VERSION_PATCH_LEColorPicker 2

// MKNetworkKit
#define COCOAPODS_POD_AVAILABLE_MKNetworkKit
#define COCOAPODS_VERSION_MAJOR_MKNetworkKit 0
#define COCOAPODS_VERSION_MINOR_MKNetworkKit 87
#define COCOAPODS_VERSION_PATCH_MKNetworkKit 0

// NGAParallaxMotion
#define COCOAPODS_POD_AVAILABLE_NGAParallaxMotion
#define COCOAPODS_VERSION_MAJOR_NGAParallaxMotion 1
#define COCOAPODS_VERSION_MINOR_NGAParallaxMotion 0
#define COCOAPODS_VERSION_PATCH_NGAParallaxMotion 2

// PopoverView
#define COCOAPODS_POD_AVAILABLE_PopoverView
#define COCOAPODS_VERSION_MAJOR_PopoverView 0
#define COCOAPODS_VERSION_MINOR_PopoverView 0
#define COCOAPODS_VERSION_PATCH_PopoverView 1

// Reachability
#define COCOAPODS_POD_AVAILABLE_Reachability
#define COCOAPODS_VERSION_MAJOR_Reachability 3
#define COCOAPODS_VERSION_MINOR_Reachability 1
#define COCOAPODS_VERSION_PATCH_Reachability 1

// StackBluriOS
#define COCOAPODS_POD_AVAILABLE_StackBluriOS
#define COCOAPODS_VERSION_MAJOR_StackBluriOS 0
#define COCOAPODS_VERSION_MINOR_StackBluriOS 0
#define COCOAPODS_VERSION_PATCH_StackBluriOS 1

// WEPopover
#define COCOAPODS_POD_AVAILABLE_WEPopover
#define COCOAPODS_VERSION_MAJOR_WEPopover 0
#define COCOAPODS_VERSION_MINOR_WEPopover 0
#define COCOAPODS_VERSION_PATCH_WEPopover 1


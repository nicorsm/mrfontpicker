//
//  UIImage+Rotation.h
//  BlurredLines
//
//  Created by Nico on 09/11/13.
//  Copyright (c) 2013 Mr. APPs srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Rotation)

- (UIImage *)rotate90DegWithMultiple:(int)degrees;

@end

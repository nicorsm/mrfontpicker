//
//  WebEngine.h
//  BlurredLines
//
//  Created by Nicola Giancecchi on 27/02/14.
//  Copyright (c) 2014 Mr. APPs srl. All rights reserved.
//

#import "MKNetworkEngine.h"

typedef void (^CompletitionBlock)(BOOL completed);
@interface WebEngine : MKNetworkEngine

+ (WebEngine *)sharedEngine;

-(MKNetworkOperation*)downloadJsonDataAndOnComplete:(CompletitionBlock)completeBlock;

@end

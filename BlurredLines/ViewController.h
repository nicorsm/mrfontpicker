//
//  ViewController.h
//  BlurredLines
//
//  Created by Nico on 03/11/13.
//  Copyright (c) 2013 Mr. APPs srl. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    int selectedIndex;
}
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmented;
- (IBAction)indexChanged:(UISegmentedControl *)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblTakeImage;
- (IBAction)openCredits:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imgBlurredLines;

@end

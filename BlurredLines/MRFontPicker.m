//
//  MRFontPicker.m
//  MRFontPicker
//
//  Created by Nico on 13/11/13.
//  Copyright (c) 2013 Mr. APPs srl. All rights reserved.
//

#import "MRFontPicker.h"
#import "UIColor+HexString.h"
#import "UIImage+Color.h"
#import "UIImage+ColorChangeCategory.h"

@interface MRFontPicker ()
{
    int selectedIndexFontPicker;
    int selectedSubIndexFontPicker;
}


@end
@implementation MRFontPicker

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)init{
    self = [super initWithNibName:@"MRFontPicker" bundle:nil];
    if(self){
        
        alphabeticalFonts = [[UIFont familyNames] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    }
    
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Align right
    [alignRightButton setImage:[UIImage imageNamed:@"align_right"] forState:UIControlStateNormal];
    [alignRightButton setImage:[UIImage imageNamed:@"align_right"] forState:UIControlStateHighlighted];
    
    [alignRightButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.9 alpha:1.0]] forState:UIControlStateNormal];
    [alignRightButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.8 alpha:1.0]] forState:UIControlStateSelected];
    
    //Align center
    [alignCenterButton setImage:[UIImage imageNamed:@"align_center"] forState:UIControlStateNormal];
    [alignCenterButton setImage:[UIImage imageNamed:@"align_center"] forState:UIControlStateHighlighted];
    [alignCenterButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.9 alpha:1.0]]  forState:UIControlStateNormal];
    [alignCenterButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.8 alpha:1.0]] forState:UIControlStateSelected];
    
    //Align left
    [alignLeftButton setImage:[UIImage imageNamed:@"align_left"] forState:UIControlStateNormal];
    [alignLeftButton setImage:[UIImage imageNamed:@"align_left"] forState:UIControlStateHighlighted];
    [alignLeftButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.9 alpha:1.0]] forState:UIControlStateNormal];
    [alignLeftButton setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.8 alpha:1.0]] forState:UIControlStateSelected];

    [goBackButton setImage:[goBackButton.imageView.image imageWithColor:[UIColor colorWithWhite:0.8 alpha:1.0] ] forState:UIControlStateNormal];
    [goBackButton setImage:[goBackButton.imageView.image imageWithColor:[UIColor colorWithWhite:0.7 alpha:1.0] ] forState:UIControlStateNormal];
    
    
    [self initFontSizeScrollView];
    [self initFontColorScrollView];
    
    selectedIndexFontPicker = (int)[alphabeticalFonts indexOfObject:@"Helvetica Neue"];
    NSArray *names = [UIFont fontNamesForFamilyName:alphabeticalFonts[selectedIndexFontPicker]];
    selectedSubIndexFontPicker = (int)[names indexOfObject:@"HelveticaNeue-Thin"];
    
    [pickerView selectRow:selectedIndexFontPicker inComponent:0 animated:YES];
    [pickerView selectRow:selectedSubIndexFontPicker inComponent:1 animated:YES];
    
    if(!textAlignment){
        textAlignment = NSTextAlignmentLeft;
    }
    
}



-(void)initFontSizeScrollView{
    
    //Calculating the sizes
    if(!self.minSize){
        self.minSize = 10;
    }
    
    if(!self.maxSize){
        self.maxSize = 50;
    }
    
    
    if(!self.stepAmount){
        self.stepAmount = 2;
    }
    
    if(!fontSize){
        fontSize = [NSNumber numberWithInt:20];
    }

    sizesArray = [[NSMutableArray alloc] init];
    
    for(int i = self.minSize; i <= self.maxSize; i += self.stepAmount){
        [sizesArray addObject:[NSNumber numberWithInt:i]];
    }
    
    //I'm starting to draw the font size scrollview
    self.fontSizeScrollView.contentSize = (CGSize){44*sizesArray.count, 44};
    
    for(int i = 0; i < sizesArray.count; i++){
        UIButton *thisSizeBtn = [[UIButton alloc] initWithFrame:(CGRect){(i*44)+6, 6, 32, 32}];
        
        NSNumber *thisSizeNumber = sizesArray[i];
    	[thisSizeBtn setTitle:[NSString stringWithFormat:@"%d", thisSizeNumber.intValue] forState:UIControlStateNormal];
        [[thisSizeBtn titleLabel] setTextAlignment:NSTextAlignmentCenter];
        [[thisSizeBtn titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-Thin" size:16.0]];
        [thisSizeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [thisSizeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [thisSizeBtn addTarget:self action:@selector(updateFontSize:) forControlEvents:UIControlEventTouchUpInside];
        thisSizeBtn.tag = i+1;
        thisSizeBtn.layer.cornerRadius = 16;
        thisSizeBtn.layer.masksToBounds = YES;
        thisSizeBtn.layer.borderColor = [UIColor colorWithWhite:0.8 alpha:1.0].CGColor;
        thisSizeBtn.layer.borderWidth = 1;
        [thisSizeBtn setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:0.8 alpha:1.0]] forState:UIControlStateSelected];
        [thisSizeBtn setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithWhite:1.0 alpha:1.0]] forState:UIControlStateNormal];
        
        [self.fontSizeScrollView addSubview:thisSizeBtn];
    }
    
}



-(void)initFontColorScrollView{
    
    
    if(!fontColor){
        fontColor = [UIColor whiteColor];
    }
    
    if(!colorsArray){
        colorsArray = @[[UIColor blackColor], [UIColor whiteColor], [UIColor colorWithHexString:@"#268BD2"], [UIColor colorWithHexString:@"#2AA198"], [UIColor colorWithHexString:@"#859900"], [UIColor colorWithHexString:@"#8DC63F"], [UIColor colorWithHexString:@"#FFF200"], [UIColor colorWithHexString:@"#B58900"], [UIColor colorWithHexString:@"#F09218"], [UIColor colorWithHexString:@"#DC322F"], [UIColor colorWithHexString:@"#D33682"], [UIColor colorWithHexString:@"#6C71C4"], [UIColor colorWithHexString:@"#FDF6E3"], [UIColor colorWithHexString:@"#839496"], [UIColor colorWithHexString:@"#002B36"], [UIColor colorWithHexString:@"#3A3A3A"]];
    }
    
    //I'm starting to draw the font size scrollview
    self.colorsScrollView.contentSize = (CGSize){44*colorsArray.count, 44};
    
    for(int i = 0; i < colorsArray.count; i++){
        UIButton *thisColorBtn = [[UIButton alloc] initWithFrame:(CGRect){(i*44)+6, 6, 32, 32}];
        
        [thisColorBtn setBackgroundColor:(UIColor*)colorsArray[i]];
        [thisColorBtn addTarget:self action:@selector(updateFontColor:) forControlEvents:UIControlEventTouchUpInside];
        thisColorBtn.tag = i+1;
        thisColorBtn.layer.cornerRadius = 16;
        thisColorBtn.layer.masksToBounds = YES;
        thisColorBtn.layer.borderColor = [UIColor colorWithWhite:0.8 alpha:1.0].CGColor;
        thisColorBtn.layer.borderWidth = 1;
        [self.colorsScrollView addSubview:thisColorBtn];
    }
    
}



-(void)updateFontSize:(UIButton*)sender{
    
    NSNumber *selectedSize = sizesArray[sender.tag-1];
    fontSize = selectedSize;
    [self updateTextAppearance];
    
    for(int i = 0; i < sizesArray.count; i++){
        UIButton *b = (UIButton*)[self.fontSizeScrollView viewWithTag:i+1];
        [b setSelected:(i == sender.tag-1)];
    }
}



-(void)updateFontColor:(UIButton*)sender{
    
    fontColor = (UIColor*)colorsArray[sender.tag-1];
    [self updateTextAppearance];
    
}


-(void)updateTextAppearance{
    /*
    [[fontChoosingButton titleLabel] setFont:[UIFont fontWithName:fontName size:fontSize.floatValue]];
    [fontChoosingButton setTitleColor:fontColor forState:UIControlStateNormal];
    [[fontChoosingButton titleLabel] setTextAlignment:textAlignment];
    */
    if([self.delegate respondsToSelector:@selector(updateTextPickerPropertiesWithFontName:fontSize:fontColor:andTextAlignment:)]){
        [self.delegate updateTextPickerPropertiesWithFontName:fontName fontSize:fontSize fontColor:fontColor andTextAlignment:textAlignment];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)setLeftAlignment {
    textAlignment = NSTextAlignmentLeft;
    alignLeftButton.selected = YES;
    alignRightButton.selected = NO;
    alignCenterButton.selected = NO;
    [self updateTextAppearance];
}
- (IBAction)setCenterAlignment {
    textAlignment = NSTextAlignmentCenter;
    alignLeftButton.selected = NO;
    alignRightButton.selected = NO;
    alignCenterButton.selected = YES;
    [self updateTextAppearance];
}
- (IBAction)setRightAlignment {
    textAlignment = NSTextAlignmentRight;
    alignLeftButton.selected = NO;
    alignRightButton.selected = YES;
    alignCenterButton.selected = NO;
    [self updateTextAppearance];
}
- (IBAction)openFontToolbox {
    
    [UIView animateWithDuration:0.3 animations:^{
        
        backgroundScrollview.contentOffset = CGPointMake(320,0);
        
        
    }];
}
- (IBAction)goBack {
    
    [UIView animateWithDuration:0.3 animations:^{
        
        backgroundScrollview.contentOffset = CGPointMake(0,0);
        
        
    }];
}

#pragma mark - Picker delegate methods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}


-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
   
    if(component == 0){
        	return alphabeticalFonts.count;
        } else {
            NSArray *names = [UIFont fontNamesForFamilyName:alphabeticalFonts[selectedIndexFontPicker]];
            
            return names.count ;
        }return 0;
    
    return 0;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if(component == 0){
        
        selectedIndexFontPicker = (int)row;
    	[pickerView reloadComponent:1];
    }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if(component == 0){
            return alphabeticalFonts[row];
    } else if (component == 1){
            NSArray *names = [UIFont fontNamesForFamilyName:alphabeticalFonts[selectedIndexFontPicker]];
            NSString *nomeCompleto = names[row];
        [fontChoosingButton setTitle:nomeCompleto forState:UIControlStateNormal];
            fontName = nomeCompleto;
            [self updateTextAppearance];
            NSString *nomeDaEscludere = [alphabeticalFonts[selectedIndexFontPicker] stringByReplacingOccurrencesOfString:@" " withString:@""];
            return [nomeCompleto stringByReplacingOccurrencesOfString:[nomeDaEscludere stringByAppendingString:@"-"] withString:@""];
        }
    return @"";
}

@end

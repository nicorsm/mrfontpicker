//
//  GradientViewController.m
//  BlurredLines
//
//  Created by Nico on 05/11/13.
//  Copyright (c) 2013 Mr. APPs srl. All rights reserved.
//

#import "GradientViewController.h"
#import <StackBluriOS/UIImage+StackBlur.h>
#import <QuartzCore/QuartzCore.h>
#import <GPUImage/GPUImage.h>
#import "UIImage+ColorChangeCategory.h"
#import <Social/Social.h>

@interface GradientViewController ()

@end

@implementation GradientViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.renderView.backgroundColor = [UIColor whiteColor];
    
    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI * 0.5);
    self.gradientPositionSlider.transform = trans;
    self.topColor = [UIColor redColor];
    self.bottomColor = [UIColor orangeColor];
    

    [self updateGradient];
    [self drawBubbles];
    
    
    [self changeButtonsColor];
    
    self.renderView.parallaxIntensity = 20;
    
    [[[GAI sharedInstance] defaultTracker] send:[[[GAIDictionaryBuilder createAppView] set:@"Acid Gradient"
                                                                                    forKey:kGAIScreenName] build]];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismissViewController:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)topGradientColorTapped:(UIButton *)sender {
    
    if (!self.topColorPopover) {
        
        UIView *pickerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 213, 238)];
        
        RSColorPickerView *colorPick = [[RSColorPickerView alloc] initWithFrame:CGRectMake(9, 9, 197, 197)];
        colorPick.delegate = self;

        RSBrightnessSlider *bright = [[RSBrightnessSlider alloc] initWithFrame:CGRectMake(9, 210, 197, 18)];
        bright.colorPicker = colorPick;
        bright.value = 1.0;
        
        [pickerView addSubview:colorPick];
        [pickerView addSubview:bright];
        
        selected = 1;
        
        [colorPick setSelectionColor:self.topColor];
        
        self.topColorPopover = [[PopoverView alloc] init];
        self.topColorPopover.delegate = self;
        
		[self.topColorPopover showAtPoint:CGPointMake(160,topButton.center.y) inView:self.view withContentView:pickerView];
        
        
    } else {
        [self.topColorPopover dismiss];
        self.topColorPopover = nil;
        selected = 0;
    }
}
- (IBAction)sliderDidChange:(UISlider *)sender {
    
    [self updateGradient];
}

- (IBAction)bottomGradientColorTapped:(UIButton *)sender {
    
    if (!self.bottomColorPopover) {
        
        
        UIView *pickerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 213, 238)];
        
        RSColorPickerView *colorPick = [[RSColorPickerView alloc] initWithFrame:CGRectMake(9, 9, 197, 197)];
        
        colorPick.delegate = self;
        
        RSBrightnessSlider *bright = [[RSBrightnessSlider alloc] initWithFrame:CGRectMake(9, 210, 197, 18)];
        bright.colorPicker = colorPick;
        bright.value = 1.0;
        
        [pickerView addSubview:colorPick];
        [pickerView addSubview:bright];
        
        selected = 2;
        
        [colorPick setSelectionColor:self.bottomColor];
        
        
        self.bottomColorPopover = [[PopoverView alloc] init];
        self.bottomColorPopover.delegate = self;
        
		[self.bottomColorPopover showAtPoint:CGPointMake(160,bottomButton.center.y) inView:self.view withContentView:pickerView];
        
    } else {
        [self.bottomColorPopover dismiss];
        self.bottomColorPopover = nil;
        selected = 0;
    }
}

- (IBAction)saveToCameraRoll:(id)sender {
    
    //UIGraphicsBeginImageContext(self.renderView.frame.size);
    UIGraphicsBeginImageContextWithOptions(self.renderView.frame.size, YES, 0.0);
    [[self.renderView layer] renderInContext:UIGraphicsGetCurrentContext()];
    screenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    // produce your image
    NSData* imageData =  UIImagePNGRepresentation(screenshot);     // get png representation
    UIImage* pngImage = [UIImage imageWithData:imageData];    // rewrap
    UIImageWriteToSavedPhotosAlbum(pngImage, nil, nil, nil);  // save to photo album
    
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Yeah!" message:@"Image successfully saved into your Camera Roll." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:@"Share on Facebook", @"Share on Twitter", nil];
    [alert show];
    
    
}

- (IBAction)didSwitchTexture:(UISwitch *)sender {
    
    if(sender.on){
        
        self.textureView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bubbles.png"]];
    } else {
        self.textureView.backgroundColor = [UIColor clearColor];
    }
}

- (IBAction)didSwitchBubbles:(UISwitch *)sender {
    
    
    self.bubbleView.hidden = !self.bubbleView.hidden;

}

-(void)popoverViewDidDismiss:(PopoverView *)popoverView{
    
    self.topColorPopover = nil;
    self.bottomColorPopover = nil;
}


/*
-(void)popoverControllerDidDismissPopover:(WEPopoverController *)popoverController{
    
    self.topColorPopover = nil;
    self.bottomColorPopover = nil;
}

-(BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)popoverController{
    return YES;
}
*/

-(void)colorPickerDidChangeSelection:(RSColorPickerView *)cp{

	if(selected==1){
        self.topColor = cp.selectionColor;
    } else if(selected == 2){
        self.bottomColor = cp.selectionColor;
    }
    
    
    [self updateGradient];
    
    [self changeButtonsColor];
}

-(void)updateGradient{
    

    CGFloat hmin = 0, smin = 0, bmin = 0, amin = 0;
    
    
    if([self.topColor getHue:&hmin saturation:&smin brightness:&bmin alpha:&amin]){
        self.gradientPositionSlider.minimumTrackTintColor = [UIColor colorWithHue:hmin saturation:smin brightness:bmin-0.2 alpha:amin];
    }
    
    
    CGFloat hmax = 0, smax = 0, bmax = 0, amax = 0;
    if([self.bottomColor getHue:&hmax saturation:&smax brightness:&bmax alpha:&amax]){
        self.gradientPositionSlider.maximumTrackTintColor = [UIColor colorWithHue:hmax saturation:smax brightness:bmax-0.2 alpha:amax];
    }
    
    self.theGradient.gradientOrientation = GRKGradientOrientationDown;
    
    self.theGradient.gradientColors = @[self.topColor, self.bottomColor];
    
    self.theGradient.gradientLocations = @[@(self.gradientPositionSlider.value), @1.0];
    
    [self.theGradient updateGradient];

}


-(void)drawBubbles{
    
    UIView *view = [[UIView alloc] initWithFrame:self.bubbleView.frame];
    
    
    for(int i = 0; i < 14; i++){
        
        double bubbleAlpha = (arc4random()%60 + 20);
        double bubbleXPos = arc4random()%(int)view.bounds.size.width - 50;
        double bubbleYPos = arc4random()%(int)view.bounds.size.height - 50;
        double bubbleRadius = (arc4random()%150 + 50);
        

        UIImageView *circle = [[UIImageView alloc] initWithFrame:(CGRect){bubbleXPos,bubbleYPos,bubbleRadius,bubbleRadius}];
     	circle.image = [UIImage imageNamed:[NSString stringWithFormat:@"bubbles-%d", i%7]];
        circle.alpha = bubbleAlpha/100 + 0.2;
        
        [self.bubbleView addSubview:circle];
        
    }
    
    
    
}



-(void)changeButtonsColor{
    
    UIButton *btnTopSx = (UIButton*)[self.view viewWithTag:1];
    //[btnTopSx setTitleColor:self.topColor forState:UIControlStateNormal];
    [btnTopSx setTitleColor:self.topColor forState:UIControlStateHighlighted];
	[btnTopSx setTitleColor:self.topColor forState:UIControlStateSelected];
    
    UIButton *btnTopDx = (UIButton*)[self.view viewWithTag:2];
    //[btnTopDx setTitleColor:self.topColor forState:UIControlStateNormal];
    [btnTopDx setTitleColor:self.topColor forState:UIControlStateHighlighted];
	[btnTopDx setTitleColor:self.topColor forState:UIControlStateSelected];
    
    UISwitch *sw1 = (UISwitch*)[self.view viewWithTag:3];
    sw1.onTintColor = self.bottomColor;
    
    
    UISwitch *sw2 = (UISwitch*)[self.view viewWithTag:4];
    sw2.onTintColor = self.bottomColor;
    
    /*
    UILabel *lb1 = (UILabel*)[self.view viewWithTag:5];
    lb1.textColor = self.bottomColor;
    
    
    UILabel *lb2 = (UILabel*)[self.view viewWithTag:6];
    lb2.textColor = self.bottomColor;
    */
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex == 1){ //Facebook
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
        {
            SLComposeViewController *fbSheetOBJ = [SLComposeViewController
                                                   composeViewControllerForServiceType:SLServiceTypeFacebook];
            [fbSheetOBJ setInitialText:@"Wallpaper created with Blurred Life - Check out! http://bit.ly/blurredlife #blurredlife"];
            [fbSheetOBJ addImage:screenshot];
            [fbSheetOBJ setCompletionHandler:^(SLComposeViewControllerResult result){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self dismissViewController:nil];
                });
            }];
            [self presentViewController:fbSheetOBJ animated:YES completion:nil];
        } else {
            [self dismissViewController:nil];
        }
    } else if(buttonIndex == 2){ //Twitter
        
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            SLComposeViewController *tweetSheetOBJ = [SLComposeViewController
                                                      composeViewControllerForServiceType:SLServiceTypeTwitter];
            [tweetSheetOBJ setInitialText:@"Wallpaper created with Blurred Life - Check out! http://bit.ly/blurredlife #blurredlife"];
            [tweetSheetOBJ addImage:screenshot];
            [tweetSheetOBJ setCompletionHandler:^(SLComposeViewControllerResult result){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self dismissViewController:nil];
                });
            }];
            [self presentViewController:tweetSheetOBJ animated:YES completion:nil];
        } else {
            [self dismissViewController:nil];
        }
    } else if(buttonIndex==0){
    	
        [self dismissViewController:nil];
    }
    
}


@end

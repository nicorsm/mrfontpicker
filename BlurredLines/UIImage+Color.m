//
//  UIImage+Color.m
//  Incontri
//
//  Created by Nico on 24/07/13.
//  Copyright (c) 2013 Mr. APPs s.r.l. All rights reserved.
//

#import "UIImage+Color.h"

@implementation UIImage (Color)

+(UIImage*)imageFromColor:(UIColor*)color withRect:(CGRect)rectMake
{
    CGRect rect = CGRectMake(0,0,rectMake.size.width,rectMake.size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

+(UIImage*)imageWithColor:(UIColor*)color{
    return [UIImage imageFromColor:color withRect:(CGRect){0,0,1,1}];
}

@end

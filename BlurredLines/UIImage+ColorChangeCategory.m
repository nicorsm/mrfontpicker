

#import "UIImage+ColorChangeCategory.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIImage (ColorChangeCategory)

-(UIImage *)imageWithColor:(UIColor *)color{
    
	// begin a new image context, to draw our colored image onto
	
	char majorVersion = [[[UIDevice currentDevice] systemVersion] characterAtIndex: 0];
	if (majorVersion == '2' || majorVersion == '3')
		UIGraphicsBeginImageContext(self.size);
	else
		UIGraphicsBeginImageContextWithOptions(self.size, NO, [UIScreen mainScreen].scale);
	
	
	
	
    // get a reference to that context we created
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetShouldAntialias(context, YES);
	CGContextSetInterpolationQuality( context, kCGInterpolationHigh );
    // set the fill color
	[color setFill];
	
    // translate/flip the graphics context (for transforming from CG* coords to UI* coords
	CGContextTranslateCTM(context, 0, self.size.height);
	CGContextScaleCTM(context, 1.0, -1.0);
	
    // set the blend mode to color burn, and the original image
	CGContextSetBlendMode(context, kCGBlendModeCopy);
	CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
	CGContextDrawImage(context, rect, self.CGImage);
	
	
	
    // set a mask that matches the shape of the image, then draw (color burn) a colored rectangle
	CGContextClipToMask(context, rect, self.CGImage);
	CGContextAddRect(context, rect);
	CGContextDrawPath(context,kCGPathFill);
	
    // generate a new UIImage from the graphics context we drew onto
	UIImage *coloredImg = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	

    //return the color-burned image
	return coloredImg;


}

+ (UIImage *)imageWithPatternColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}



-(UIImage *)imageWithGradient{
	
		// begin a new image context, to draw our colored image onto
	
	char majorVersion = [[[UIDevice currentDevice] systemVersion] characterAtIndex: 0];
	if (majorVersion == '2' || majorVersion == '3')
		UIGraphicsBeginImageContext(self.size);
	else
		UIGraphicsBeginImageContextWithOptions(self.size, NO, [UIScreen mainScreen].scale);
	
		// get a reference to that context we created
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	
		// translate/flip the graphics context (for transforming from CG* coords to UI* coords
	CGContextTranslateCTM(context, 0, self.size.height);
	CGContextScaleCTM(context, 1.0, -1.0);
	
		// set the blend mode to color burn, and the original image
	CGContextSetBlendMode(context, kCGBlendModeHue);
	CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
	
	CGContextDrawImage(context, rect, self.CGImage);
	
		// Create a gradient from white to black
	CGFloat colors [] = {
		1.0, 1.0, 1.0, 1.0,
		0.0, 0.0, 0.0, 1.0
	};
	
	CGColorSpaceRef baseSpace = CGColorSpaceCreateDeviceRGB();
	CGGradientRef gradient = CGGradientCreateWithColorComponents(baseSpace, colors, NULL, 2);
	CFRelease(baseSpace);
	CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
	CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
	
	CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
	CGGradientRelease(gradient), gradient = NULL;
	
		// set a mask that matches the shape of the image, then draw (color burn) a colored rectangle
	CGContextClipToMask(context, rect, self.CGImage);
	CGContextAddRect(context, rect);
	CGContextDrawPath(context,kCGPathFill);
	
		// generate a new UIImage from the graphics context we drew onto
	UIImage *coloredImg = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
		//return the color-burned image
	return coloredImg;
}



- (UIImage *)imageScaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [self drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


+ (UIImage *)imageNamed:(NSString *)name withColor:(UIColor *)color{
	
	UIImage* img = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:name ofType:@"png"]];
	
	return [img imageWithColor:color];
}


@end

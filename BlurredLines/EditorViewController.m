//
//  EditorViewController.m
//  BlurredLines
//
//  Created by Nico on 03/11/13.
//  Copyright (c) 2013 Mr. APPs srl. All rights reserved.
//

#import "EditorViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <GPUImage/GPUImage.h>
#import <StackBluriOS/UIImage+StackBlur.h>
#import "UIImage+ColorChangeCategory.h"
#import "UIImage+Rotation.h"
#import "UIColor+HexString.h"
#import <Social/Social.h>


@interface EditorViewController ()

@end

@implementation EditorViewController

- (IBAction)saveToCameraRoll:(id)sender {
    theTextView.backgroundColor = [UIColor clearColor];
    // produce your image
    NSData* imageData =  UIImagePNGRepresentation([self imageWithView:imageView]);     // get png representation
    UIImage* pngImage = [UIImage imageWithData:imageData];    // rewrap
    UIImageWriteToSavedPhotosAlbum(pngImage, nil, nil, nil);  // save to photo album
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Yeah!" message:@"Image successfully saved into your Camera Roll." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:@"Share on Facebook", @"Share on Twitter", nil];
    [alert show];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil originalImage:(UIImage*)original
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        NSData *data = UIImageJPEGRepresentation(original, 0.9999);
        //Valori di default
        self.originalImage = [UIImage imageWithData:data];
        
    	blurAmount = 50;
        brightnessAmount = 0.0;
        saturationAmount = 1.0;
        filterIndex = 0;
        rotationIndex = 0;
        
        
    }
    return self;
}

-(void)deselectAll{
    blurBtn.selected = NO;
    brightnessBtn.selected = NO;
    saturationBtn.selected = NO;
    textBtn.selected = NO;
    rotateBtn.selected = NO;
}

- (IBAction)dismiss:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)selectBlur:(UIButton *)sender {
    [self deselectAll];
    sender.selected = YES;
    
    slider.minimumTrackTintColor = [UIColor colorWithHexString:@"#C20349"];
    slider.thumbTintColor = [UIColor colorWithHexString:@"#C20349"];
    slider.minimumValueImage = [UIImage imageNamed:@"pallino_nero"];
    slider.maximumValueImage = [UIImage imageNamed:@"pallino_sfuocato"];
    slider.minimumValue = 0;
    slider.maximumValue = 100;
    slider.value = blurAmount;
    [self sliderValueChanged:slider];
    [self showLabelDescriptionWithText:@"Blur"];
    
}

- (IBAction)selectBrightness:(UIButton *)sender {
    [self deselectAll];
    sender.selected = YES;
    
    slider.minimumTrackTintColor = [UIColor colorWithHexString:@"#01C3D9"];
    slider.thumbTintColor = [UIColor colorWithHexString:@"#01C3D9"];
    slider.minimumValueImage = [UIImage imageNamed:@"pallino_nero"];
    slider.maximumValueImage = [UIImage imageNamed:@"pallino_bianco"];
    slider.minimumValue = -1;
    slider.maximumValue = 1;
    slider.value = brightnessAmount;
    [self showLabelDescriptionWithText:@"Brightness"];
}

- (IBAction)selectContrast:(UIButton *)sender {
    
    sender.selected = YES;
}

- (IBAction)selectFilters:(UIButton *)sender {
    sender.selected = YES;
}

- (IBAction)selectRotate:(UIButton *)sender {
    rotation++;
    [self rotateImage];
    //self.originalImage = [self.originalImage rotate90deg];
    //[self sliderValueChanged:slider];
    
    [self showLabelDescriptionWithText:@"Rotation"];
}

-(void)rotateImage{
    
    [imageView setImage:self.finalImage];
    
    for(int i = 0; i < rotation % 4; i++){
        [imageView setImage:[self.finalImage rotate90DegWithMultiple:rotation%4]];
    }
    
}


- (IBAction)selectSaturation:(UIButton *)sender {
    [self deselectAll];
    sender.selected = YES;
    
    slider.minimumTrackTintColor = [UIColor colorWithHexString:@"#F28A05"];
    slider.thumbTintColor = [UIColor colorWithHexString:@"#F28A05"];
    slider.minimumValueImage = [UIImage imageNamed:@"pallino_bn"];
    slider.maximumValueImage = [UIImage imageNamed:@"pallino_colore"];
    slider.minimumValue = 0;
    slider.maximumValue = 2;
    slider.value = saturationAmount;
    [self showLabelDescriptionWithText:@"Saturation"];
}

-(void)showLabelDescriptionWithText:(NSString*)stringa{
    
    self.effectDescr.text = stringa;
    self.effectDescr.alpha = 1;
    [self performSelector:@selector(hideLabel) withObject:nil afterDelay:2.0];
}

-(void)hideLabel{
    [UIView animateWithDuration:0.5 animations:^{
        self.effectDescr.alpha = 0;
    }];
    
}


- (IBAction)sliderValueChanged:(UISlider *)sender {
    
    
	if(blurBtn.selected){
        blurAmount = sender.value;
        
        [activityIndicator setHidden:NO];
        [activityIndicator startAnimating];
        
        [self performSelectorInBackground:@selector(blurWrapper:) withObject:@{@"image":self.originalImage,@"amount":[NSNumber numberWithFloat:sender.value]}];
        
    } else if(brightnessBtn.selected){
        brightnessAmount = sender.value;
        [self calculateOtherEffects];
    }
    else if(saturationBtn.selected){
        saturationAmount = sender.value;
        [self calculateOtherEffects];
    }
    
    
}

- (IBAction)selectText:(UIButton *)sender {
    
    
    UIButton *btn = (UIButton*)[self.view viewWithTag:100];
    btn.selected = NO;
    
    [uniPicker.view removeFromSuperview];
    
    [self showLabelDescriptionWithText:@"Text"];
    
    
    if(theTextView == nil){
        // (2) Create a second resizable view with a UIImageView as the content.
        
        
        CGRect textFrame = CGRectMake(50, 200, 200, 200);
        resizableView = [[SPUserResizableView alloc] initWithFrame:textFrame];
        theTextView = [[UITextView alloc] initWithFrame:resizableView.bounds];
        theTextView.backgroundColor = [UIColor clearColor];
        theTextView.textColor = [UIColor whiteColor];
        theTextView.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:20.0];
        theTextView.text = @"Insert here the text.";
        theTextView.delegate = self;
        theTextView.scrollEnabled = NO;
        theTextView.tintColor = [UIColor lightGrayColor];
        //theTextView.inputAccessoryView = [self drawToolbar];
        theTextView.autocorrectionType = UITextAutocorrectionTypeNo;
        
        
        [imageView addSubview:theTextView];
        
        resizableView.contentView = theTextView;
        resizableView.delegate = self;
        
        [imageView addSubview:resizableView];
        
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideEditingHandles)];
        [gestureRecognizer setDelegate:self];
        [self.view addGestureRecognizer:gestureRecognizer];
        
    }
    
   // [theTextView becomeFirstResponder];
    theTextView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.2];
    
    
    [theTextView becomeFirstResponder];
    
    self.textPanelEditor.hidden = NO;
    
    
}

-(void)updateTextPickerPropertiesWithFontName:(NSString *)fontName fontSize:(NSNumber *)fontSize fontColor:(UIColor *)fontColor andTextAlignment:(NSTextAlignment)textAlignment{
    
    theTextView.font = [UIFont fontWithName:fontName size:fontSize.floatValue];
    theTextView.textAlignment = textAlignment;
    theTextView.textColor = fontColor;
    
}

- (void)userResizableViewDidBeginEditing:(SPUserResizableView *)userResizableView {
    [resizableView hideEditingHandles];
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([resizableView hitTest:[touch locationInView:resizableView] withEvent:nil]) {
        return NO;
    }
    return YES;
}

- (void)hideEditingHandles {
    [resizableView hideEditingHandles];
}


-(void)textViewDidChange:(UITextView *)textView{
    //[theTextView sizeToFit];
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    
    theTextView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.2];
    [self selectText:textBtn];
}


-(void)dismissKeyboard{
    [theTextView resignFirstResponder];
    
    //theTextView.backgroundColor = [UIColor clearColor];
}

-(void)calculateOtherEffects{
    
    //Applico brightness
    
    GPUImageBrightnessFilter *bright = [[GPUImageBrightnessFilter alloc] init];
    bright.brightness = brightnessAmount;
    self.finalImage = [bright imageByFilteringImage:self.blurredImage];
    
    
    //Applico saturation
    GPUImageSaturationFilter *sat = [[GPUImageSaturationFilter alloc] init];
    sat.saturation = saturationAmount;
    self.finalImage = [sat imageByFilteringImage:self.finalImage];
    
    
    //imageView.image = self.finalImage;
    [self rotateImage];
    
    [activityIndicator setHidden:YES];
    [activityIndicator stopAnimating];
    
}

- (IBAction)selectTint:(UIButton *)sender {
    sender.selected = YES;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
	self.effectDescr.layer.cornerRadius = 10;
    self.effectDescr.alpha = 0;
    
    
    [self selectBlur:blurBtn];
    
    /*
    LEColorPicker *colorPicker = [[LEColorPicker alloc] init];
    LEColorScheme *colorScheme = [colorPicker colorSchemeFromImage:self.originalImage];
    slider.minimumTrackTintColor = [colorScheme primaryTextColor];
    
    [self changeButtonsColor:colorScheme.primaryTextColor inView:self.view];
    */
    [self setButtonsColor];
    
    imageView.parallaxIntensity = 20;
    
    [[[GAI sharedInstance] defaultTracker] send:[[[GAIDictionaryBuilder createAppView] set:@"Editor"
                                                                                    forKey:kGAIScreenName] build]];
    
    /*
    blurBtn.imageView.image = [UIImage imageNamed:@"blur@2x.png"];
    
    saturationBtn.imageView.image = [UIImage imageNamed:@"saturation@2x.png"];
    
    brightnessBtn.imageView.image = [UIImage imageNamed:@"brightness@2x.png"];
    
    rotateBtn.imageView.image = [UIImage imageNamed:@"rotate@2x.png"];*/
}

-(void)setButtonsColor{
    
    
    
    [blurBtn setImage:[blurBtn.imageView.image imageWithColor:[UIColor blackColor]] forState:UIControlStateNormal];
    [blurBtn setImage:[blurBtn.imageView.image imageWithColor:[UIColor colorWithHexString:@"#C20349"]] forState:UIControlStateHighlighted];
    [blurBtn setImage:[blurBtn.imageView.image imageWithColor:[UIColor colorWithHexString:@"#C20349"]] forState:UIControlStateSelected];
    
    
    [brightnessBtn setImage:[brightnessBtn.imageView.image imageWithColor:[UIColor blackColor]] forState:UIControlStateNormal];
    [brightnessBtn setImage:[brightnessBtn.imageView.image imageWithColor:[UIColor colorWithHexString:@"#01C3D9"]] forState:UIControlStateHighlighted];
    [brightnessBtn setImage:[brightnessBtn.imageView.image imageWithColor:[UIColor colorWithHexString:@"#01C3D9"]] forState:UIControlStateSelected];
    
    
    
    [textBtn setImage:[textBtn.imageView.image imageWithColor:[UIColor blackColor]] forState:UIControlStateNormal];
    [textBtn setImage:[textBtn.imageView.image imageWithColor:[UIColor colorWithHexString:@"#A2D927"]] forState:UIControlStateHighlighted];
    [textBtn setImage:[textBtn.imageView.image imageWithColor:[UIColor colorWithHexString:@"#A2D927"]] forState:UIControlStateSelected];
    
    [saturationBtn setImage:[saturationBtn.imageView.image imageWithColor:[UIColor blackColor]] forState:UIControlStateNormal];
    [saturationBtn setImage:[saturationBtn.imageView.image imageWithColor:[UIColor colorWithHexString:@"#F28A05"]] forState:UIControlStateHighlighted];
    [saturationBtn setImage:[saturationBtn.imageView.image imageWithColor:[UIColor colorWithHexString:@"#F28A05"]] forState:UIControlStateSelected];
    
    [checkMarkBtn setImage:[checkMarkBtn.imageView.image imageWithColor:[UIColor blackColor]] forState:UIControlStateNormal];
    [checkMarkBtn setImage:[checkMarkBtn.imageView.image imageWithColor:[UIColor colorWithHexString:@"#A2D927"]] forState:UIControlStateHighlighted];
    [checkMarkBtn setImage:[checkMarkBtn.imageView.image imageWithColor:[UIColor colorWithHexString:@"#A2D927"]] forState:UIControlStateSelected];
    
    
    [paintBtn setImage:[paintBtn.imageView.image imageWithColor:[UIColor blackColor]] forState:UIControlStateNormal];
    [paintBtn setImage:[[paintBtn imageForState:UIControlStateHighlighted] imageWithColor:[UIColor colorWithHexString:@"#A2D927"]] forState:UIControlStateHighlighted];
    [paintBtn setImage:[[paintBtn imageForState:UIControlStateSelected] imageWithColor:[UIColor colorWithHexString:@"#A2D927"]] forState:UIControlStateSelected];
    
    [rotateBtn setImage:[rotateBtn.imageView.image imageWithColor:[UIColor blackColor]] forState:UIControlStateNormal];
    [rotateBtn setImage:[rotateBtn.imageView.image imageWithColor:[UIColor colorWithHexString:@"#61C791"]] forState:UIControlStateHighlighted];
    
}

-(void)changeButtonsColor:(UIColor*)color inView:(UIView*)view{
    
    if([view isKindOfClass:[UIButton class]] && view.tag != 100){
        UIButton *btn = (UIButton*)view;
        [btn setTitleColor:color forState:UIControlStateHighlighted];
        [btn setTitleColor:color forState:UIControlStateSelected];
        [btn setImage:[btn.imageView.image imageWithColor:[UIColor blackColor]] forState:UIControlStateNormal];
        [btn setImage:[btn.imageView.image imageWithColor:color] forState:UIControlStateHighlighted];
        [btn setImage:[btn.imageView.image imageWithColor:color] forState:UIControlStateSelected];
    }
    
    if(view.tag == 100){
        UIButton *btn = (UIButton*)view;
        [btn setTitleColor:color forState:UIControlStateHighlighted];
         [btn setTitleColor:color forState:UIControlStateSelected];
        [btn setImage:[[btn imageForState:UIControlStateNormal] imageWithColor:[UIColor blackColor]] forState:UIControlStateNormal];
        [btn setImage:[[btn imageForState:UIControlStateHighlighted] imageWithColor:color] forState:UIControlStateHighlighted];
        [btn setImage:[[btn imageForState:UIControlStateSelected] imageWithColor:color] forState:UIControlStateSelected];
    }
    
    for (UIView *sview in view.subviews){
		[self changeButtonsColor:color inView:sview];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)blurWrapper:(NSDictionary*)args{
    
    self.blurredImage = [self blur:[args objectForKey:@"image"] withAmountOfBlur:[[args objectForKey:@"amount"] floatValue]];
    
    [self calculateOtherEffects];
    
}


- (UIImage*) blur:(UIImage*)theImage withAmountOfBlur:(float)blur
{
    
    return [theImage stackBlur:(int)blur];
}

- (UIImage *)imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}


- (IBAction)switchProperties:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    
    if(sender.selected){
        
        [theTextView resignFirstResponder];
        
        if(uniPicker == nil){
            uniPicker = [[MRFontPicker alloc] init];
            uniPicker.delegate = self;
            uniPicker.minSize = 15;
            uniPicker.maxSize = 60;
            uniPicker.stepAmount = 3;
            
            CGRect bounds = [UIScreen mainScreen].bounds;
            uniPicker.view.frame = CGRectMake(0,bounds.size.height-176,320,176);
            [self addChildViewController:uniPicker];
            
    	}
        
        [self.view addSubview:uniPicker.view];
        

    } else {
        [uniPicker.view removeFromSuperview];
        [theTextView becomeFirstResponder];
    }
}

- (IBAction)done:(id)sender {
    
    [uniPicker.view removeFromSuperview];
    [theTextView resignFirstResponder];
    self.textPanelEditor.hidden = YES;
    
    
    [theTextView resignFirstResponder];
    UIButton *btn = (UIButton*)[self.view viewWithTag:100];
    btn.selected = NO;
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex == 1){ //Facebook
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
        {
            SLComposeViewController *fbSheetOBJ = [SLComposeViewController
                                                      composeViewControllerForServiceType:SLServiceTypeFacebook];
            [fbSheetOBJ setInitialText:@"Wallpaper created with Blurred Life - Check out! http://bit.ly/blurredlife #blurredlife"];
            [fbSheetOBJ addImage:[self imageWithView:imageView]];
            [fbSheetOBJ setCompletionHandler:^(SLComposeViewControllerResult result){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self dismiss:nil];
                });
            }];
            [self presentViewController:fbSheetOBJ animated:YES completion:nil];
        } else {
            [self dismiss:nil];
        }
    } else if(buttonIndex == 2){ //Twitter
        
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            SLComposeViewController *tweetSheetOBJ = [SLComposeViewController
                                                      composeViewControllerForServiceType:SLServiceTypeTwitter];
            [tweetSheetOBJ setInitialText:@"Wallpaper created with Blurred Life - Check out! http://bit.ly/blurredlife #blurredlife"];
            [tweetSheetOBJ addImage:[self imageWithView:imageView]];
            [tweetSheetOBJ setCompletionHandler:^(SLComposeViewControllerResult result){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self dismiss:nil];
                });
            }];
            [self presentViewController:tweetSheetOBJ animated:YES completion:nil];
        } else {
            [self dismiss:nil];
        }
    } else if(buttonIndex==0){
    	
        [self dismiss:nil];
    }
    
}
@end

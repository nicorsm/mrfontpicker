//
//  GradientViewController.h
//  BlurredLines
//
//  Created by Nico on 05/11/13.
//  Copyright (c) 2013 Mr. APPs srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GRKGradientView/GRKGradientView.h>
#import <PopoverView/PopoverView.h>
#import "RSColorPickerView.h"
#import "RSBrightnessSlider.h"
#import "UIColor+HexString.h"
#import <NGAParallaxMotion/NGAParallaxMotion.h>

@interface GradientViewController : UIViewController<PopoverViewDelegate,RSColorPickerViewDelegate>{
    int selected;
    IBOutlet UIButton *topButton;
    IBOutlet UIButton *bottomButton;
    UIImage *screenshot;
}

@property (strong, nonatomic) IBOutlet UIView *bubbleView;
@property (strong, nonatomic) IBOutlet GRKGradientView *theGradient;
- (IBAction)dismissViewController:(UIButton *)sender;
- (IBAction)topGradientColorTapped:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UISlider *gradientPositionSlider;
- (IBAction)sliderDidChange:(UISlider *)sender;

- (IBAction)bottomGradientColorTapped:(UIButton *)sender;
- (IBAction)saveToCameraRoll:(id)sender;
- (IBAction)didSwitchTexture:(UISwitch *)sender;
- (IBAction)didSwitchBubbles:(UISwitch *)sender;


@property (strong, nonatomic) IBOutlet UIView *renderView;
@property(strong,nonatomic) IBOutlet PopoverView *topColorPopover;
@property(strong,nonatomic) IBOutlet PopoverView *bottomColorPopover;

@property(strong,nonatomic) UIColor *topColor;
@property(strong,nonatomic) UIColor *bottomColor;

@property (strong, nonatomic) IBOutlet UIView *textureView;

@property(strong,nonatomic) RSColorPickerView *topColorPicker;

@property(strong,nonatomic) RSColorPickerView *bottomColorPicker;


@property(strong,nonatomic) RSBrightnessSlider *topColorBright;

@property(strong,nonatomic) RSBrightnessSlider *bottomColorBright;

@end

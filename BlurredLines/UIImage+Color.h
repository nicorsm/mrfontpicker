//
//  UIImage+Color.h
//  Incontri
//
//  Created by Nico on 24/07/13.
//  Copyright (c) 2013 Mr. APPs s.r.l. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Color)

+(UIImage*)imageFromColor:(UIColor*)color withRect:(CGRect)rectMake;
+(UIImage*)imageWithColor:(UIColor*)color;

@end

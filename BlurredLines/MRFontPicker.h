//
//  MRFontPicker.h
//  MRFontPicker
//
//  Created by Nico on 13/11/13.
//  Copyright (c) 2013 Mr. APPs srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MRFontPickerDelegate<NSObject>

-(void)updateTextPickerPropertiesWithFontName:(NSString*)fontName fontSize:(NSNumber*)fontSize fontColor:(UIColor*)fontColor andTextAlignment:(NSTextAlignment)textAlignment;

@end

@interface MRFontPicker : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>{
    
    NSString *fontName;
    NSNumber *fontSize;
    UIColor *fontColor;
    NSTextAlignment textAlignment;
    
    IBOutlet UIButton *fontChoosingButton;
    
    NSMutableArray *sizesArray;
    NSArray *colorsArray;
    
    IBOutlet UIScrollView *backgroundScrollview;
    
    IBOutlet UIButton *goBackButton;
    IBOutlet UIButton *alignRightButton;
    IBOutlet UIButton *alignCenterButton;
    IBOutlet UIButton *alignLeftButton;
    IBOutlet UIPickerView *pickerView;
    NSArray *alphabeticalFonts;
}

@property unsigned int minSize;
@property unsigned int maxSize;
@property unsigned int stepAmount;

@property (strong, nonatomic) IBOutlet UIScrollView *fontSizeScrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *colorsScrollView;

@property (nonatomic, weak) id<MRFontPickerDelegate> delegate;

-(id)init;
-(void)updateFontSize:(UIButton*)sender;


@end

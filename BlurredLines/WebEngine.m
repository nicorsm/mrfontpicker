//
//  WebEngine.m
//  BlurredLines
//
//  Created by Nicola Giancecchi on 27/02/14.
//  Copyright (c) 2014 Mr. APPs srl. All rights reserved.
//

#import "WebEngine.h"
#import <JSONKit-NoWarning/JSONKit.h>

@implementation WebEngine

static WebEngine * _engine;
+ (WebEngine *)sharedEngine {
	static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _engine = [[WebEngine alloc] initWithHostName:@"www.blurredlifeapp.com"];
        [_engine useCache];
    });
	return _engine;
}

-(MKNetworkOperation*)downloadJsonDataAndOnComplete:(CompletitionBlock)completeBlock{
    
    MKNetworkOperation *configOp = [self operationWithPath:@"config/appconfig.json" params:nil  httpMethod:@"GET"];
    
    [configOp addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        if (self.isReachable && completedOperation.isCachedResponse) {
            return ;
        }
        
        NSDictionary* json = [completedOperation.responseString objectFromJSONString];
        if(json){
            [[NSUserDefaults standardUserDefaults] setObject:[json objectForKey:@"appGratisEnabled"] forKey:@"appGratisEnabled"];
            [[NSUserDefaults standardUserDefaults] setObject:[json objectForKey:@"appGratisURL"] forKey:@"appGratisURL"];
            completeBlock(YES);
        }
        else{
            NSLog(@"Failed to reach server");
            completeBlock(NO);
        }
    } errorHandler:^(MKNetworkOperation *completedOperation, NSError *error) {
        NSLog(@"Failed to connect to server");
        completeBlock(NO);
    }];
    
    [self enqueueOperation:configOp];
    
    return configOp;
}




@end

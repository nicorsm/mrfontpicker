//
//  CreditsViewController.h
//  BlurredLines
//
//  Created by Nico on 11/11/13.
//  Copyright (c) 2013 Mr. APPs srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreditsViewController : UIViewController{
    
    IBOutlet UIView *blackBg;
    IBOutlet UIButton *btnDiscoverApps;
}
- (IBAction)dismiss:(id)sender;
- (IBAction)openMrAppsSite:(id)sender;

@end

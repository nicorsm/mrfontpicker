

#import <UIKit/UIKit.h>

@interface UIImage (ColorChangeCategory)

+ (UIImage *)imageNamed:(NSString *)name withColor:(UIColor *)color;
-(UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage *)imageWithPatternColor:(UIColor *)color;
-(UIImage *)imageWithGradient;
- (UIImage *)imageScaledToSize:(CGSize)newSize;

@end

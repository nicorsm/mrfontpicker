//
//  CreditsViewController.m
//  BlurredLines
//
//  Created by Nico on 11/11/13.
//  Copyright (c) 2013 Mr. APPs srl. All rights reserved.
//

#import "CreditsViewController.h"

@interface CreditsViewController ()

@end

@implementation CreditsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    [[[GAI sharedInstance] defaultTracker] send:[[[GAIDictionaryBuilder createAppView] set:@"Credits"
                                                                                    forKey:kGAIScreenName] build]];
    
    btnDiscoverApps.layer.borderColor = [UIColor whiteColor].CGColor;
    btnDiscoverApps.layer.borderWidth = 1.0;
    btnDiscoverApps.layer.cornerRadius = 8;
    btnDiscoverApps.layer.masksToBounds = YES;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dismiss:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)openApps:(id)sender {
    NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/us/artist/mr.-apps-s.r.l./id493882589"];
    [[UIApplication sharedApplication] openURL:url];
    
}
- (IBAction)openMrAppsSite:(id)sender {
    NSURL *url = [NSURL URLWithString:@"http://www.mr-apps.com"];
    [[UIApplication sharedApplication] openURL:url];

}
- (IBAction)openBlurredLifeAppSite:(id)sender {
    NSURL *url = [NSURL URLWithString:@"http://www.blurredlifeapp.com"];
    [[UIApplication sharedApplication] openURL:url];
}


- (IBAction)openEmail:(id)sender {
    NSURL *url = [NSURL URLWithString:@"mailto:support@mr-apps.com"];
    [[UIApplication sharedApplication] openURL:url];
    
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
@end

//
//  EditorViewController.h
//  BlurredLines
//
//  Created by Nico on 03/11/13.
//  Copyright (c) 2013 Mr. APPs srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPUserResizableView.h"
#import <LEColorPicker/LEColorPicker.h>
#import <NGAParallaxMotion/NGAParallaxMotion.h>
#import "MRFontPicker.h"

@interface EditorViewController : UIViewController<UITextViewDelegate,UIGestureRecognizerDelegate, UIPickerViewDelegate,UIPickerViewDataSource,MRFontPickerDelegate, UIAlertViewDelegate>{

    IBOutlet UIImageView *imageView;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UISlider *slider;
    
    float blurAmount;
    float brightnessAmount;
    float contrastAmount;
    float tintAmount;
    float saturationAmount;
    float filterIndex;
    float rotationIndex;

    IBOutlet UIButton *blurBtn;
    
    IBOutlet UIButton *brightnessBtn;
    
    //IBOutlet UIButton *contrastBtn;
    
    IBOutlet UIButton *rotateBtn;
    IBOutlet UIButton *textBtn;
    //IBOutlet UIButton *filterBtn;
    IBOutlet UIButton *saturationBtn;
    //IBOutlet UIButton *tintBtn;
    
    IBOutlet UIButton *checkMarkBtn;
    IBOutlet UIButton *paintBtn;
    SPUserResizableView *resizableView;
    UITextView *theTextView;
    
    MRFontPicker *uniPicker;
    
    int rotation;
    
}
- (IBAction)saveToCameraRoll:(id)sender;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil originalImage:(UIImage*)original;
- (IBAction)dismiss:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *effectDescr;



- (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer;

- (IBAction)selectBlur:(UIButton *)sender;
- (IBAction)selectBrightness:(UIButton *)sender;
- (IBAction)selectContrast:(UIButton *)sender;
- (IBAction)selectTint:(UIButton *)sender;
- (IBAction)selectFilters:(UIButton *)sender;
- (IBAction)selectRotate:(UIButton *)sender;
- (IBAction)selectSaturation:(UIButton *)sender;
- (IBAction)sliderValueChanged:(UISlider *)sender;
- (IBAction)selectText:(UIButton *)sender;



@property(strong,nonatomic) UIImage *originalImage;
@property(strong,nonatomic) UIImage *blurredImage;
@property(strong,nonatomic) UIImage *finalImage;
@property (strong, nonatomic) IBOutlet UIView *textPanelEditor;

- (IBAction)switchProperties:(UIButton *)sender;
- (IBAction)done:(id)sender;

@end

//
//  AppDelegate.h
//  BlurredLines
//
//  Created by Nico on 03/11/13.
//  Copyright (c) 2013 Mr. APPs srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MKNetworkKit/MKNetworkKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(strong,nonatomic) MKNetworkOperation *configOp;
@property(strong,nonatomic) MKNetworkEngine *eng;
@end

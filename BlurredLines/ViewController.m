//
//  ViewController.m
//  BlurredLines
//
//  Created by Nico on 03/11/13.
//  Copyright (c) 2013 Mr. APPs srl. All rights reserved.
//

#import "ViewController.h"
#import "EditorViewController.h"
#import "GradientViewController.h"
#import "CreditsViewController.h"
#import "WebEngine.h"
#import "KVBrowserViewController.h"

@interface ViewController (){
    
    IBOutlet UIView *appGratisView;
    IBOutlet UIButton *checkOutBtn;
    IBOutlet UIButton *dismissBtn;
    IBOutlet UIView *appGratisBlackView;
    IBOutlet UIView *dismissView;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    float offset = 250;
    
    self.imgBlurredLines.alpha = 0.0;
    self.lblTakeImage.alpha = 0.0;
    self.segmented.alpha = 0.0;
    
    self.imgBlurredLines.frame = (CGRect){self.imgBlurredLines.frame.origin.x-offset, self.imgBlurredLines.frame.origin.y, self.imgBlurredLines.frame.size};
    
    
    [UIView animateWithDuration:1.0 animations:^{
        
        self.imgBlurredLines.alpha = 1.0;
        
        self.imgBlurredLines.frame = (CGRect){self.imgBlurredLines.frame.origin.x+offset, self.imgBlurredLines.frame.origin.y, self.imgBlurredLines.frame.size};
        
        
    } completion:^(BOOL finished) {
        
        
        [UIView animateWithDuration:1.0 animations:^{
            
            self.lblTakeImage.alpha = 1.0;
            self.segmented.alpha = 1.0;
            
        }];

        
    }];
    
    
    [[[GAI sharedInstance] defaultTracker] send:[[[GAIDictionaryBuilder createAppView] set:@"Home page"
                                                                                    forKey:kGAIScreenName] build]];
    
    appGratisBlackView.layer.cornerRadius = 12;
    appGratisBlackView.layer.masksToBounds = YES;
    appGratisBlackView.exclusiveTouch = YES;
    
    checkOutBtn.layer.borderWidth = 1;
    checkOutBtn.layer.borderColor = [UIColor redColor].CGColor;
    checkOutBtn.layer.cornerRadius = 5;
    checkOutBtn.layer.masksToBounds = YES;
    
    dismissBtn.layer.borderWidth = 1;
    dismissBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    dismissBtn.layer.cornerRadius = 5;
    dismissBtn.layer.masksToBounds = YES;
    
    dismissView.layer.borderWidth = 1;
    dismissView.layer.borderColor = [UIColor whiteColor].CGColor;
    dismissView.layer.cornerRadius = 15;
    dismissView.layer.masksToBounds = YES;
    
    
    [[WebEngine sharedEngine] downloadJsonDataAndOnComplete:^(BOOL completed) {
        [self showAppGratis];
    }];
    
}

-(void)showAppGratis{
    
    NSString *key = [NSString stringWithFormat:@"appGratisOpened_%@", [[[NSUserDefaults standardUserDefaults] objectForKey:@"appGratisURL"] lastPathComponent]];
    
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"appGratisEnabled"] isEqual:@(YES)] && [[NSUserDefaults standardUserDefaults] objectForKey:key] == nil){
        self.segmented.userInteractionEnabled = NO;
        appGratisView.hidden = NO;
        appGratisView.alpha = 0;
        [self.view addSubview:appGratisView];
        [UIView animateWithDuration:0.5 animations:^{
            appGratisView.alpha = 1;
            
        } completion:^(BOOL finished) {
        }];
    }
}

- (IBAction)openAppGratis:(id)sender {
    
    [self dismissMessage:nil];
    
    KVBrowserViewController *bro = [[KVBrowserViewController alloc] initWithUrl:[[NSUserDefaults standardUserDefaults] objectForKey:@"appGratisURL"]];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:bro];
    [self presentViewController:nav animated:YES completion:nil];
}

- (IBAction)dismissMessage:(id)sender {
    
    
    NSString *key = [NSString stringWithFormat:@"appGratisOpened_%@", [[[NSUserDefaults standardUserDefaults] objectForKey:@"appGratisURL"] lastPathComponent]];
    
    [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:key];
    self.segmented.userInteractionEnabled = YES;
    [UIView animateWithDuration:0.5 animations:^{
        appGratisView.alpha = 0;
        
    } completion:^(BOOL finished) {
        appGratisView.hidden = YES;
        [appGratisView removeFromSuperview];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)indexChanged:(UISegmentedControl *)sender {
    
    if(sender.selectedSegmentIndex == 2){
        
        GradientViewController *grad = [[GradientViewController alloc] initWithNibName:@"GradientViewController" bundle:nil];
        [self presentViewController:grad animated:YES completion:nil];
        
        
    } else {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = NO;
        picker.sourceType = sender.selectedSegmentIndex==0?UIImagePickerControllerSourceTypeCamera:UIImagePickerControllerSourceTypePhotoLibrary;
        
        if(sender.selectedSegmentIndex == 0){
            
            [[[GAI sharedInstance] defaultTracker] send:[[[GAIDictionaryBuilder createAppView] set:@"Use Camera"
                                                                                            forKey:kGAIEventLabel] build]];
            
        } else if(sender.selectedSegmentIndex == 1){
            
            
            [[[GAI sharedInstance] defaultTracker] send:[[[GAIDictionaryBuilder createAppView] set:@"Use Library"
                                                                                            forKey:kGAIEventLabel] build]];
        }
        [self presentViewController:picker animated:YES completion:NULL];
        
        
    }
    
    sender.selectedSegmentIndex = UISegmentedControlNoSegment;

}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        if([info objectForKey:UIImagePickerControllerOriginalImage]){
            
            
            UIImage *editedImage = (UIImage*)[info objectForKey:UIImagePickerControllerOriginalImage];
            
            UIImage *correctedImg = editedImage;
            
            if([info objectForKey:UIImagePickerControllerMediaMetadata] != nil)
            {
                // foto presa da fotocamera
                correctedImg = [self scaleAndRotateImage:correctedImg];
            }
            
                EditorViewController *editor = [[EditorViewController alloc] initWithNibName:@"EditorViewController" bundle:nil originalImage:correctedImg];
                
                [self presentViewController:editor animated:YES completion:nil];
            
        }

    }];
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
        
}

- (UIImage *)scaleAndRotateImage:(UIImage *)image {
	int kMaxResolution = 4000; // Or whatever
	
    CGImageRef imgRef = image.CGImage;
	
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
	
	
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
	
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
			
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
			
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
			
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
			
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
			
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
			
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
			
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
			
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
			
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
			
    }
	
    UIGraphicsBeginImageContext(bounds.size);
	
    CGContextRef context = UIGraphicsGetCurrentContext();
	
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
	
    CGContextConcatCTM(context, transform);
	
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
	
    return imageCopy;
}

- (IBAction)openCredits:(id)sender {
    CreditsViewController *cred = [[CreditsViewController alloc] initWithNibName:@"CreditsViewController" bundle:nil];
    [self presentViewController:cred animated:YES completion:nil];
}


- (IBAction)openMrAppsSite:(id)sender {
    NSURL *url = [NSURL URLWithString:@"http://www.mr-apps.com"];
    [[UIApplication sharedApplication] openURL:url];
    
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
@end

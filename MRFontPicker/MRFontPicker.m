//
//  MRFontPicker.m
//  MRFontPicker
//
//  Created by Nico on 13/11/13.
//  Copyright (c) 2013 Mr. APPs srl. All rights reserved.
//

#import "MRFontPicker.h"

@interface MRFontPicker ()
{
    int selectedIndexFontPicker;
}


@end
@implementation MRFontPicker

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)init{
    self = [super initWithNibName:@"MRFontPicker" bundle:nil];
    if(self){
        
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    fontName = @"HelveticaNeue-Thin";

    [self initFontSizeScrollView];
    [self initFontColorScrollView];
    
    
    if(!textAlignment){
        textAlignment = NSTextAlignmentLeft;
    }
}



-(void)initFontSizeScrollView{
    
    //Calculating the sizes
    if(!self.minSize){
        self.minSize = 10;
    }
    
    if(!self.maxSize){
        self.maxSize = 50;
    }
    
    
    if(!self.stepAmount){
        self.stepAmount = 2;
    }
    
    if(!fontSize){
        fontSize = [NSNumber numberWithInt:20];
    }

    sizesArray = [[NSMutableArray alloc] init];
    
    for(int i = self.minSize; i <= self.maxSize; i += self.stepAmount){
        [sizesArray addObject:[NSNumber numberWithInt:i]];
    }
    
    //I'm starting to draw the font size scrollview
    self.fontSizeScrollView.contentSize = (CGSize){44*sizesArray.count, 44};
    
    for(int i = 0; i < sizesArray.count; i++){
        UIButton *thisSizeBtn = [[UIButton alloc] initWithFrame:(CGRect){i*44, 0, 44, 44}];
        
        NSNumber *thisSizeNumber = sizesArray[i];
    	[thisSizeBtn setTitle:[NSString stringWithFormat:@"%d", thisSizeNumber.intValue] forState:UIControlStateNormal];
        [[thisSizeBtn titleLabel] setTextAlignment:NSTextAlignmentCenter];
        [thisSizeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [thisSizeBtn addTarget:self action:@selector(updateFontSize:) forControlEvents:UIControlEventTouchUpInside];
        thisSizeBtn.tag = i+1;
        [self.fontSizeScrollView addSubview:thisSizeBtn];
    }
    
}



-(void)initFontColorScrollView{
    
    
    if(!fontColor){
        fontColor = [UIColor whiteColor];
    }
    
    if(!colorsArray){
        colorsArray = @[[UIColor blackColor], [UIColor whiteColor], [UIColor greenColor], [UIColor blueColor], [UIColor redColor], [UIColor yellowColor], [UIColor orangeColor], [UIColor purpleColor], [UIColor brownColor], [UIColor grayColor], [UIColor lightGrayColor]];
    }
    
    //I'm starting to draw the font size scrollview
    self.colorsScrollView.contentSize = (CGSize){44*colorsArray.count, 44};
    
    for(int i = 0; i < colorsArray.count; i++){
        UIButton *thisColorBtn = [[UIButton alloc] initWithFrame:(CGRect){(i*44)+6, 6, 32, 32}];
        
        [thisColorBtn setBackgroundColor:(UIColor*)colorsArray[i]];
        [thisColorBtn addTarget:self action:@selector(updateFontColor:) forControlEvents:UIControlEventTouchUpInside];
        thisColorBtn.tag = i+1;
        [self.colorsScrollView addSubview:thisColorBtn];
    }
    
}



-(void)updateFontSize:(UIButton*)sender{
    
    NSNumber *selectedSize = sizesArray[sender.tag-1];
    fontSize = selectedSize;
    [self updateTextAppearance];
    
}



-(void)updateFontColor:(UIButton*)sender{
    
    fontColor = (UIColor*)colorsArray[sender.tag-1];
    [self updateTextAppearance];
    
}


-(void)updateTextAppearance{
    [[fontChoosingButton titleLabel] setFont:[UIFont fontWithName:fontName size:fontSize.floatValue]];
    [fontChoosingButton setTitleColor:fontColor forState:UIControlStateNormal];
    [[fontChoosingButton titleLabel] setTextAlignment:textAlignment];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)setLeftAlignment {
    textAlignment = NSTextAlignmentLeft;
    [self updateTextAppearance];
}
- (IBAction)setCenterAlignment {
    textAlignment = NSTextAlignmentCenter;
    [self updateTextAppearance];
}
- (IBAction)setRightAlignment {
    textAlignment = NSTextAlignmentRight;
    [self updateTextAppearance];
}
- (IBAction)openFontToolbox {
    
    [UIView animateWithDuration:0.5 animations:^{
        
        backgroundScrollview.contentOffset = CGPointMake(320,0);
        
        
    }];
}
- (IBAction)goBack {
    
    [UIView animateWithDuration:0.5 animations:^{
        
        backgroundScrollview.contentOffset = CGPointMake(0,0);
        
        
    }];
}

#pragma mark - Picker delegate methods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}


-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
   
    if(component == 0){
        	return [UIFont familyNames].count;
        } else {
            NSArray *names = [UIFont fontNamesForFamilyName:[UIFont familyNames][selectedIndexFontPicker]];
            
            return names.count ;
        }return 0;
    
    return 0;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if(component == 0){
        
        selectedIndexFontPicker = (int)row;
    	[pickerView reloadComponent:1];
    }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if(component == 0){
            return [UIFont familyNames][row];
    } else if (component == 1){
            NSArray *names = [UIFont fontNamesForFamilyName:[UIFont familyNames][selectedIndexFontPicker]];
            NSString *nomeCompleto = names[row];
            fontName = nomeCompleto;
            [self updateTextAppearance];
            NSString *nomeDaEscludere = [[UIFont familyNames][selectedIndexFontPicker] stringByReplacingOccurrencesOfString:@" " withString:@""];
            return [nomeCompleto stringByReplacingOccurrencesOfString:[nomeDaEscludere stringByAppendingString:@"-"] withString:@""];
        }
    return @"";
}

@end

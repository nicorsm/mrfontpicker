//
//  MRAppDelegate.h
//  MRFontPicker
//
//  Created by Nico on 13/11/13.
//  Copyright (c) 2013 Mr. APPs srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MRAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

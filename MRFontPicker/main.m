//
//  main.m
//  MRFontPicker
//
//  Created by Nico on 13/11/13.
//  Copyright (c) 2013 Mr. APPs srl. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MRAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MRAppDelegate class]));
    }
}

//
//  MRViewController.m
//  MRFontPicker
//
//  Created by Nico on 13/11/13.
//  Copyright (c) 2013 Mr. APPs srl. All rights reserved.
//

#import "MRViewController.h"
#import "MRFontPicker.h"

@interface MRViewController ()

@end

@implementation MRViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    MRFontPicker *fontPicker = [[MRFontPicker alloc] init];
    [self addChildViewController:fontPicker];
    [self.view addSubview:fontPicker.view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  MRFontPicker.h
//  MRFontPicker
//
//  Created by Nico on 13/11/13.
//  Copyright (c) 2013 Mr. APPs srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MRFontPicker : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>{
    
    NSString *fontName;
    NSNumber *fontSize;
    UIColor *fontColor;
    NSTextAlignment textAlignment;
    
    IBOutlet UIButton *fontChoosingButton;
    
    NSMutableArray *sizesArray;
    NSArray *colorsArray;
    
    IBOutlet UIScrollView *backgroundScrollview;
}

@property unsigned int minSize;
@property unsigned int maxSize;
@property unsigned int stepAmount;

@property (strong, nonatomic) IBOutlet UIScrollView *fontSizeScrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *colorsScrollView;


-(id)init;
-(void)updateFontSize:(UIButton*)sender;


@end
